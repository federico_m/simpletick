#pragma once

#include <JuceHeader.h>

class Metronome {
public:
	enum class PlayState
	{
		Playing, Stopped
	};

	enum Signatures {
		TreQuarti = 1, TreOttavi, QuattroQuarti, CinqueQuarti, SeiQuarti, SeiOttavi, SetteQuarti, SetteOttavi, NoveQuarti, NoveOttavi, DodiciOttavi
	};

	enum AccentType {
		Simple = 1, None
	};

	Metronome();
	~Metronome();

	void prepareToPlay(int samplesPerBlockExpected, double sampleRate);
	void getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill, Atomic<bool>& showTimeVisualization);

	void play();
	void stop();

	bool isPlaying() const;
	int setBpm(int bpm);
	int getBpm()const;
	int getMinBpm() const;
	int getMaxBpm() const;

	int getBeat() const;

	void setAccentType(const Metronome::AccentType& accentType);

	void setSignature(const Signatures& signature);
private:
	PlayState _playState;
	int _bpm, _numSamples,_minBpm,_maxBpm;
	double _sampleRate, _samplePerBpm, _samplePlayed;


	bool _firstBeat;
	uint8 _subdivisionNum, _subdivisionDen;
	Atomic<uint8> _accent;
	Atomic<AccentType> _accentType;

	AudioFormatManager _formatManager;
	std::unique_ptr<AudioFormatReaderSource> _readerSourceAccent, _readerSourceNormal;

	inline void updateSamplePerBpm();
};