#include "MainComponent.h"

//==============================================================================
MainComponent::MainComponent() : _tapTime{ "Tap" }, _playBtn{ "Play" }, _signature{ "Metro" }, _selectAccentType{ "selectAccentType" }, _showTimeVisualization{ false }
{
	addAndMakeVisible(_timeSignatureLbl);
	_timeSignatureLbl.setText("Time Signature", dontSendNotification);
	addAndMakeVisible(_accentsLbl);
	_accentsLbl.setText("Accent", dontSendNotification);
	addAndMakeVisible(_tempoLbl);
	_tempoLbl.setText("Tempo", dontSendNotification);

	_playBtn.setToggleState(true, dontSendNotification);
	addAndMakeVisible(_playBtn);
	_playBtn.setWantsKeyboardFocus(true);
	_playBtn.addShortcut(KeyPress{ ' ' });

	_playBtn.onClick = [this]() {
		if (_metronome.isPlaying())
		{
			//stop
			_metronome.stop();
			_playBtn.setButtonText("Play");
			_signature.setEnabled(true);
			_timeVisualizationLbl.setText("", NotificationType::dontSendNotification);
			_showTimeVisualization.set(false);
		}
		else
		{
			//play
			_metronome.play();
			_playBtn.setButtonText("Stop");
			_signature.setEnabled(false);
		}
	};

	_tapTime.setToggleState(true, dontSendNotification);
	addAndMakeVisible(_tapTime);

	_tapTime.onClick = [this]() {
		int64 now = Time::currentTimeMillis();

		if (now - _lastTap < 1500) {
			//se l'ultimo tap è avvenuto meno di un secondo e mezzo fa
			auto bpm = (int)(60000.0 / (now - _lastTap));
			bpm = _metronome.setBpm(bpm);
			_txtEditorBpm.setText(String{ bpm });
		}
		_lastTap = now;

	};



	addAndMakeVisible(_txtEditorBpm);
	_txtEditorBpm.setEnabled(true);
	_txtEditorBpm.setText("120");
	_txtEditorBpm.setInputFilter(new TextEditor::LengthAndCharacterRestriction{ 3,"1234567890" }, true);
	_txtEditorBpm.setJustification(Justification::centred);

	auto computeNewBpm = 
	_txtEditorBpm.onReturnKey = [this]() {
		_playBtn.grabKeyboardFocus();
	};
	_txtEditorBpm.onFocusLost = [this]() {
		auto bpm = std::stoi(_txtEditorBpm.getText().toStdString());
		bpm = _metronome.setBpm(bpm);
		_txtEditorBpm.setText(String{ bpm });
	};


	addAndMakeVisible(_signature);
	_signature.setJustificationType(Justification::centred);
	_signature.addItem("3/4", Metronome::Signatures::TreQuarti);
	_signature.addItem("3/8", Metronome::Signatures::TreOttavi);
	_signature.addItem("4/4", Metronome::Signatures::QuattroQuarti);
	_signature.addItem("5/4", Metronome::Signatures::CinqueQuarti);
	_signature.addItem("6/4", Metronome::Signatures::SeiQuarti);
	_signature.addItem("6/8", Metronome::Signatures::SeiOttavi);
	_signature.addItem("7/4", Metronome::Signatures::SetteQuarti);
	_signature.addItem("7/8", Metronome::Signatures::SetteOttavi);
	_signature.addItem("9/4", Metronome::Signatures::NoveQuarti);
	_signature.addItem("9/8", Metronome::Signatures::NoveOttavi);
	_signature.addItem("12/8", Metronome::Signatures::DodiciOttavi);
	_signature.onChange = [this]() {
		_metronome.setSignature(static_cast<Metronome::Signatures>(_signature.getSelectedId()));
	};


	_signature.setSelectedId(Metronome::Signatures::QuattroQuarti);

	addAndMakeVisible(_selectAccentType);
	_selectAccentType.setJustificationType(Justification::centred);
	_selectAccentType.addItem("Simple", Metronome::AccentType::Simple);
	_selectAccentType.addItem("None", Metronome::AccentType::None);
	_selectAccentType.onChange = [this]() {
		_metronome.setAccentType(static_cast<Metronome::AccentType>(_selectAccentType.getSelectedId()));
	};
	_selectAccentType.setSelectedId(Metronome::AccentType::Simple);

	addAndMakeVisible(_timeVisualizationLbl);
	_timeVisualizationLbl.setFont(Font{ 70, Font::FontStyleFlags::bold });
	_timeVisualizationLbl.setJustificationType(Justification::centred);

	startTimer(50);

	setSize(300, 350);

	if (RuntimePermissions::isRequired(RuntimePermissions::recordAudio)
		&& !RuntimePermissions::isGranted(RuntimePermissions::recordAudio))
	{
		RuntimePermissions::request(RuntimePermissions::recordAudio,
			[&](bool granted) { if (granted)  setAudioChannels(2, 2); });
	}
	else
	{
		setAudioChannels(2, 2);
	}
}

MainComponent::~MainComponent() {
	shutdownAudio();
}

//==============================================================================
void MainComponent::prepareToPlay(int samplesPerBlockExpected, double sampleRate) {
	_metronome.prepareToPlay(samplesPerBlockExpected, sampleRate);
}

void MainComponent::getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill) {
	_metronome.getNextAudioBlock(bufferToFill, _showTimeVisualization);
}


void MainComponent::releaseResources()
{
}

//==============================================================================
void MainComponent::paint(Graphics& g)
{
	g.fillAll(getLookAndFeel().findColour(ResizableWindow::backgroundColourId));
}

void MainComponent::resized()
{
	Rectangle<int> rect = getLocalBounds();


	_playBtn.setBounds(rect.removeFromBottom(50).reduced(5));

	auto timeSigSpace = rect.removeFromTop(40).reduced(5);
	_timeSignatureLbl.setBounds(timeSigSpace.removeFromLeft(timeSigSpace.getWidth() / 2));
	timeSigSpace.removeFromLeft(5);
	_signature.setBounds(timeSigSpace);

	auto accentSpace = rect.removeFromTop(40).reduced(5);
	_accentsLbl.setBounds(accentSpace.removeFromLeft(accentSpace.getWidth() / 2));
	accentSpace.removeFromLeft(5);
	_selectAccentType.setBounds(accentSpace);

	auto tempoSpace = rect.removeFromTop(40).reduced(5);
	_tempoLbl.setBounds(tempoSpace.removeFromLeft(tempoSpace.getWidth() / 2));
	tempoSpace.removeFromLeft(5);
	_txtEditorBpm.setBounds(tempoSpace.removeFromLeft((tempoSpace.getWidth() / 2) - 5));
	tempoSpace.removeFromLeft(5);
	_tapTime.setBounds(tempoSpace);


	_timeVisualizationLbl.setBounds(rect);
}

void MainComponent::timerCallback()
{
	if (_metronome.isPlaying() && _showTimeVisualization.get()) {
		auto val = _metronome.getBeat();
		if (val == 1) {
			_timeVisualizationLbl.setColour(Label::ColourIds::textColourId, Colours::greenyellow);
		}
		else {
			_timeVisualizationLbl.removeColour(Label::ColourIds::textColourId);
		}

		_timeVisualizationLbl.setText(String{ val }, NotificationType::dontSendNotification);
	}
}