#include "Metronome.h"

Metronome::Metronome() :_playState{ PlayState::Stopped }, _bpm{ 120 }, _minBpm{ 60 }, _maxBpm{ 300 }, _sampleRate{ 44100.0 }, _numSamples{ 256 }, _firstBeat{ true }, _subdivisionNum{ 4 }, _subdivisionDen{ 4 }, _accentType{ AccentType::Simple }
{
	_formatManager.registerBasicFormats();

	//imposta il reader per gli accenti normali
	File dir{ File::getSpecialLocation(File::SpecialLocationType::currentExecutableFile).getParentDirectory() };
	auto filesList = dir.findChildFiles(File::TypesOfFileToFind::findFiles, true, "Metronome.wav");

	if (!filesList[0].exists()) {
		AlertWindow alert{ "Errore", "Suono del metronomo mancante", AlertWindow::AlertIconType::WarningIcon };

		alert.addButton("Esci", 1);

		alert.runModalLoop();
		JUCEApplication::getInstance()->systemRequestedQuit();
	}

	auto reader = _formatManager.createReaderFor(filesList[0]);

	_readerSourceNormal.reset(new AudioFormatReaderSource(reader, true));

	filesList = dir.findChildFiles(File::TypesOfFileToFind::findFiles, true, "MetronomeUp.wav");

	if (!filesList[0].exists()) {
		AlertWindow alert{ "Errore", "Suono del metronomo mancante", AlertWindow::AlertIconType::WarningIcon };

		alert.addButton("Esci", 1);

		alert.runModalLoop();
		JUCEApplication::getInstance()->systemRequestedQuit();
	}

	reader = _formatManager.createReaderFor(filesList[0]);

	_readerSourceAccent.reset(new AudioFormatReaderSource(reader, true));
}

Metronome::~Metronome()
{
	_readerSourceNormal->releaseResources();
	_readerSourceAccent->releaseResources();
}

void Metronome::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
	_sampleRate = sampleRate;
	_numSamples = samplesPerBlockExpected;

	_readerSourceNormal->prepareToPlay(samplesPerBlockExpected, sampleRate);
	_readerSourceAccent->prepareToPlay(samplesPerBlockExpected, sampleRate);
	_readerSourceNormal->setNextReadPosition(0);
	_readerSourceAccent->setNextReadPosition(0);
}

void Metronome::getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill, Atomic<bool>& showTimeVisualization)
{
	bufferToFill.clearActiveBufferRegion();

	if (_playState == PlayState::Playing) {
		_samplePlayed += bufferToFill.numSamples;

		if (_samplePlayed + bufferToFill.numSamples > _samplePerBpm) {
			//suona
			if (_accent.get() == 0 && _accentType.get() == AccentType::Simple) {
				_readerSourceAccent->setNextReadPosition(0);
				_readerSourceAccent->getNextAudioBlock(bufferToFill);
			}
			else {
				_readerSourceNormal->setNextReadPosition(0);
				_readerSourceNormal->getNextAudioBlock(bufferToFill);
			}
			_samplePlayed = _samplePlayed + bufferToFill.numSamples - _samplePerBpm;
			_accent.set((_accent.get() + 1) % _subdivisionNum);

			if (_firstBeat) {
				showTimeVisualization.set(true);
				_firstBeat = false;
			}
		}
		else if (_readerSourceAccent->getNextReadPosition() != 0 || _readerSourceNormal->getNextReadPosition() != 0) {
			if (((_accent.get() + _subdivisionNum - 1) % _subdivisionNum == 0) && _accentType.get() == AccentType::Simple) {
				_readerSourceAccent->getNextAudioBlock(bufferToFill);
			}
			else {
				_readerSourceNormal->getNextAudioBlock(bufferToFill);
			}
		}
	}
}

void Metronome::play()
{
	_playState = PlayState::Playing;
	updateSamplePerBpm();
	_samplePlayed = 0;
}

void Metronome::stop()
{
	_playState = PlayState::Stopped;
	_samplePlayed = 0;
	_firstBeat = true;
	_accent = 0;
}

bool Metronome::isPlaying() const
{
	return _playState == PlayState::Playing;
}

int Metronome::setBpm(int bpm)
{
	_bpm = std::max(bpm, _minBpm);
	_bpm = std::min(bpm, _maxBpm);

	updateSamplePerBpm();

	return _bpm;
}

int Metronome::getBpm() const
{
	return _bpm;
}

int Metronome::getMinBpm() const {
	return _minBpm;
}

int Metronome::getMaxBpm() const {
	return _maxBpm;
}

int Metronome::getBeat() const {
	return ((_accent.get() + _subdivisionNum - 1) % _subdivisionNum) + 1;
}

void Metronome::setAccentType(const Metronome::AccentType& accentType)
{
	_accentType.set(accentType);
}

void Metronome::setSignature(const Signatures& signature)
{
	switch (signature) {
	case Metronome::Signatures::TreQuarti:
		_subdivisionNum = 3;
		_subdivisionDen = 4;
		break;
	case Metronome::Signatures::TreOttavi:
		_subdivisionNum = 3;
		_subdivisionDen = 8;
		break;
	case Metronome::Signatures::QuattroQuarti:
		_subdivisionNum = 4;
		_subdivisionDen = 4;
		break;
	case Metronome::Signatures::CinqueQuarti:
		_subdivisionNum = 5;
		_subdivisionDen = 4;
		break;
	case Metronome::Signatures::SeiQuarti:
		_subdivisionNum = 6;
		_subdivisionDen = 4;
		break;
	case Metronome::Signatures::SeiOttavi:
		_subdivisionNum = 6;
		_subdivisionDen = 8;
		break;
	case Metronome::Signatures::SetteQuarti:
		_subdivisionNum = 7;
		_subdivisionDen = 4;
		break;
	case Metronome::Signatures::SetteOttavi:
		_subdivisionNum = 7;
		_subdivisionDen = 8;
		break;
	case Metronome::Signatures::NoveQuarti:
		_subdivisionNum = 9;
		_subdivisionDen = 4;
		break;
	case Metronome::Signatures::NoveOttavi:
		_subdivisionNum = 9;
		_subdivisionDen = 8;
		break;
	case Metronome::Signatures::DodiciOttavi:
		_subdivisionNum = 12;
		_subdivisionDen = 8;
		break;
	}
}

// Private ------------------------------------------------------------------------


inline void Metronome::updateSamplePerBpm()
{
	_samplePerBpm = (_sampleRate * 60 / _bpm) * (4.0 / _subdivisionDen);
}
