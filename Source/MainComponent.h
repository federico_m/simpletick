#pragma once

#include <JuceHeader.h>
#include "Metronome.h"


class MainComponent : public AudioAppComponent, public Timer
{
public:
	MainComponent();
	~MainComponent();

	void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;
	void getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill) override;
	void releaseResources() override;

	void paint(Graphics& g) override;
	void resized() override;

	void timerCallback() override;
private:
	TextButton _playBtn, _tapTime;
	ComboBox _signature, _selectAccentType;
	TextEditor _txtEditorBpm;
	Label _timeVisualizationLbl;
	Label _timeSignatureLbl, _accentsLbl, _tempoLbl;

	Atomic<bool>_showTimeVisualization;
	int64 _lastTap;

	Metronome _metronome;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(MainComponent)
};
